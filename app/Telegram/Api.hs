{-# LANGUAGE OverloadedStrings #-}
module Telegram.Api where

import qualified Network.HTTP.Client.TLS as C
import qualified Network.HTTP.Client as C
import Control.Monad.State
import Data.ByteString.Lazy hiding (last, head, null) 
import qualified Data.ByteString.Lazy as LBS (putStrLn)
import Data.Aeson
import Data.Maybe
import Control.Applicative
import Telegram.Types
import Data.Time

data TelegramState = TelegramState { http_manager :: C.Manager
                                   , token :: String
                                   , offset :: Int
                                   }

type Telegram = StateT TelegramState IO

data ApiResponse a = Ok a
                   | Err Int String

instance FromJSON a => FromJSON (ApiResponse a) where
  parseJSON (Object v) = Ok <$> v .: "result" 
                     <|> Err <$> v .: "error_code" <*> v .: "description"

unwrapResp :: ApiResponse a -> a
unwrapResp (Ok a) = a
unwrapResp (Err code desc) = error ("Error: Code " <> show code <> ". Description: " <> desc)

runTelegram :: Telegram a -> String -> IO a
runTelegram a token = do
  manager <- C.newTlsManager
  evalStateT a $ TelegramState {http_manager = manager, token = token, offset = maxBound} 

method :: FromJSON a => String -> Telegram a
method name = do
  manager <- gets http_manager
  token <- gets token
  request <- lift $ C.parseRequest ("https://api.telegram.org/bot" <> token <> "/" <> name)
  response <- lift $ C.httpLbs request manager
  return $ (unwrapResp . fromJust . decode) $ C.responseBody response

methodSend :: (ToJSON a, FromJSON b) => String -> a -> Telegram b
methodSend name reqObject = do
  manager <- gets http_manager
  token <- gets token
  request <- lift $ C.parseRequest ("https://api.telegram.org/bot" <> token <> "/" <> name) 
  response <- lift $ C.httpLbs request { C.method = "POST"
                                       , C.requestBody = C.RequestBodyLBS $ encode reqObject
                                       , C.requestHeaders = [("content-type", "application/json")] } manager
  lift $ LBS.putStrLn $ C.responseBody response
  return $ (unwrapResp . fromJust . decode) $ C.responseBody response

--getMe = method "getMe"

setOffset newOffset state = state { offset = newOffset } 

getUpdates :: Telegram [Update]
getUpdates = do
  offset <- gets offset
  updates <- methodSend "getUpdates" $ object ["offset" .= offset]
  state <- get
  unless (null updates) $ do
    modify . setOffset $ updateId (last updates) + 1 
  return updates

sendMessage :: String -> String -> Telegram Value
sendMessage text cid = methodSend "sendMessage" $ object ["chat_id" .= cid, "text" .= text]

test = runTelegram $ sequenceA $ (getUpdates >>= lift . print) <$ [1..]

testmes text cid = runTelegram (sendMessage text cid) 

{-# LANGUAGE OverloadedStrings #-}
module Telegram.Types where

import Data.Aeson
import Control.Applicative

data NotAvaible

instance Show NotAvaible where
  show _ = "Not Avaible"

data User = User { uid :: Int 
                 , isBot :: Bool
                 , firstName :: String
                 , lastName :: String
                 , username :: Maybe String
                 , language :: String
                 , isPremium :: Bool
                 , addedToAttachmentMenu :: Bool
                 } deriving Show

instance FromJSON User where
  parseJSON (Object v) = User <$> v .: "id"
                              <*> v .: "is_bot"
                              <*> v .: "first_name"
                              <*> v .:? "user_name" .!= ""
                              <*> v .:? "username"
                              <*> v .: "language_code"
                              <*> v .:? "is_premium" .!= False
                              <*> v .:? "added_to_attachment_menu" .!= False

data Message = MessageC { messageId :: Int
                        , from :: User
                      --, senderChat :: Chat
                        , date :: Int
                      --, chat :: Chat
                        , text :: String
                        } deriving Show --TODO

instance FromJSON Message where
  parseJSON (Object v) = MessageC <$> v .: "message_id" 
                                  <*> v .: "from"
                                  <*> v .: "date"
                                  <*> v .: "text"

type InlineQuery = NotAvaible

type ChosenInlineResult = NotAvaible

type CallbackQuery = NotAvaible

type ShippingQuery = NotAvaible

type PreCheckoutQuery = NotAvaible

type Poll = NotAvaible

type PollAnswer = NotAvaible

type ChatMemberUpdated = NotAvaible

type ChatJoinRequest = NotAvaible

data UpdateType = Message Message
                | EditedMessage Message
                | ChannelPost Message
                | EditedChannelPost Message
                | InlineQuery InlineQuery
                | ChosenInlineResult ChosenInlineResult
                | CallbackQuery CallbackQuery
                | ShippingQuery ShippingQuery
                | PreCheckoutQuery PreCheckoutQuery
                | Poll Poll
                | PollAnswer PollAnswer
                | ChatMemberUpdated ChatMemberUpdated
                | ChatJoinRequest ChatJoinRequest deriving Show

instance FromJSON Update where
  parseJSON (Object v) = Update 
                          <$> v .: "update_id" 
                          <*> (Message <$> v .: "message" 
                           <|> EditedMessage <$> v .: "edited_message"
                           <|> ChannelPost <$> v .: "channel_post"
                           <|> EditedChannelPost <$> v .: "edited_channel_post"
                           <|> InlineQuery <$> v .: "inline_query"
                           <|> ChosenInlineResult <$> v .: "chosen_inline_result"
                           <|> CallbackQuery <$> v .: "callback_query"
                           <|> ShippingQuery <$> v .: "shipping_query"
                           <|> PreCheckoutQuery <$> v .: "pre_checkout_query"
                           <|> Poll <$> v .: "poll"
                           <|> PollAnswer <$> v .: "poll_answer"
                           <|> ChatMemberUpdated <$> v .: "chat_member_updated"
                           <|> ChatJoinRequest <$> v .: "chat_join_request")

instance FromJSON NotAvaible where
  parseJSON _ = undefined

data Update = Update { updateId :: Int
                     , updateType :: UpdateType 
                     } deriving Show

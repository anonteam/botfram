module Bot where
import Control.Monad.Trans.Free
import Control.Monad.State
import GHC.Generics

data BotF x = GetMe (String -> x)

instance Functor BotF where
  fmap f (GetMe x) = GetMe (f . x)

type Bot = FreeT BotF (State ())

getMe :: Bot String
getMe = liftF $ GetMe id

testMonad :: Bot ()
testMonad = do
  state <- get
  --sendMessage $ "стейт --> " <> show state
  put state
  --getUser 1
  return ()

runBot :: (FreeF BotF a (Bot a) -> IO (Bot a)) -> Bot a -> () -> IO ()
runBot handler a s = do
  let (y, s2) = (runState . runFreeT) a s 
  z <- handler y
  runBot handler z s2 where